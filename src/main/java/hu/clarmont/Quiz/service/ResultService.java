package hu.clarmont.Quiz.service;

import hu.clarmont.Quiz.controller.entity.Question;
import hu.clarmont.Quiz.controller.entity.Result;
import hu.clarmont.Quiz.controller.entity.User;
import hu.clarmont.Quiz.repository.ResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResultService {
	private final ResultRepository repo;
	
	@Autowired
	public ResultService(ResultRepository repo) {
		this.repo = repo;
	}
	
	public Result addResult(User user, Question question, int points) {
		Result result = new Result();
		result.setUser(user);
		result.setQuestion(question);
		result.setPoints(points);
		repo.saveAndFlush(result);
		return result;
	}
}
