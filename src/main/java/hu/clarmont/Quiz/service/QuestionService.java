package hu.clarmont.Quiz.service;

import hu.clarmont.Quiz.controller.dto.GetQuestion;
import hu.clarmont.Quiz.controller.entity.Question;
import hu.clarmont.Quiz.controller.dto.SetQuestion;
import hu.clarmont.Quiz.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Stream;

@Service
public class QuestionService {
	private final QuestionRepository repo;
	
	@Autowired
	public QuestionService(QuestionRepository repo) {
		this.repo = repo; //Constructor Injection
		
		List<SetQuestion> questions = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			SetQuestion cache = new SetQuestion();
			cache.setQuestion(i + ". kérdés");
			cache.setAnswers(Stream.of("asd1", "asd2", "asd3", "asd4").reduce("", (appended, next) -> (appended.isEmpty() ? "" : appended + "|") + next));
			questions.add(cache);
		}
		
		repo.saveAllAndFlush(questions.stream().map(element -> {
			Question question = new Question();
			question.setQuestion(element.getQuestion());
			question.setAnswers(element.getAnswers());
			return question;
		}).toList());
	}
	
	public Question getById(long id) {
		return repo.findById(id).orElseThrow();
	}
	
	public List<Question> getAll() {
		return repo.findAll();
	}
	
	public GetQuestion getRandom() {
		List<Long> ids = repo.getIDs();
		Random rnd = new Random();
		Optional<Question> cache = repo.findById(ids.get(rnd.nextInt(0, ids.size())));
		return GetQuestion.convert(cache.orElseThrow());
	}
	
	public boolean checkAnswer(long id, String answer) {
		Question question = repo.findById(id).orElseThrow();
		return Arrays.stream(question.getAnswers().split("\\|")).toList().contains(answer.trim().toLowerCase());
	}
	
	public void createQuestion(SetQuestion setQuestion) {
		repo.saveAndFlush(Question.convert(setQuestion));
	}
}
