package hu.clarmont.Quiz.service;

import hu.clarmont.Quiz.controller.entity.User;
import hu.clarmont.Quiz.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	private final UserRepository repo;
	
	@Autowired
	public UserService(UserRepository repo) {
		this.repo = repo;
	}
	
	public User getUser(long id) {
		return repo.findById(id).orElse(null);
	}
}
