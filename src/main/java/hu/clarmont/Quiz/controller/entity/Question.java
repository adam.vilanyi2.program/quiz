package hu.clarmont.Quiz.controller.entity;

import hu.clarmont.Quiz.controller.dto.SetQuestion;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;

import java.util.Arrays;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "Question")
public class Question {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;
	
	@OneToMany(mappedBy = "question")
	List<Result> results;
	
	@NotBlank
	String question;
	
	@NotEmpty
	String answers;
	
	public static Question convert(SetQuestion setQuestion) {
		Question question = new Question();
		
		question.setQuestion(setQuestion.getQuestion());
		//trimming answers
		String answers = Arrays.stream(setQuestion.getAnswers().split("\\|")).map(answer -> answer.trim().toLowerCase()).reduce("", (appended, next) -> (appended.isEmpty() ? "" : appended + "|") + next);
		question.setAnswers(answers);
		
		return question;
	}
}
