package hu.clarmont.Quiz.controller.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "Result")
public class Result {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	long id;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "questionId", referencedColumnName = "id")
	Question question;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "userId", referencedColumnName = "id")
	User user;
	
	@NotNull
	int points;
}
