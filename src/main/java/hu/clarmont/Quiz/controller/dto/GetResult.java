package hu.clarmont.Quiz.controller.dto;

import hu.clarmont.Quiz.controller.entity.Result;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GetResult {
	GetQuestion question;
	GetUser user;
	int points;
	
	public static GetResult convert(Result result) {
		GetResult getResult = new GetResult();
		
		getResult.setUser(GetUser.convert(result.getUser()));
		getResult.setQuestion(GetQuestion.convert(result.getQuestion()));
		getResult.setPoints(result.getPoints());
		
		return getResult;
	}
}
