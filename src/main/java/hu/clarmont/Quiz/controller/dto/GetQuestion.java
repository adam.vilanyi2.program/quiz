package hu.clarmont.Quiz.controller.dto;

import hu.clarmont.Quiz.controller.entity.Question;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GetQuestion {
	long id;
	String question;
	
	public static GetQuestion convert(Question question) {
		GetQuestion getQuestion = new GetQuestion();
		
		getQuestion.setId(question.getId());
		getQuestion.setQuestion(question.getQuestion());
		
		return getQuestion;
	}
}
