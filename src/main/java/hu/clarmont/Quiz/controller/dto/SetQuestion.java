package hu.clarmont.Quiz.controller.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SetQuestion {
	@NotBlank
	String question;
	
	@NotEmpty
	String answers;
}
