package hu.clarmont.Quiz.controller.dto;

import hu.clarmont.Quiz.controller.entity.User;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GetUser {
	long id;
	
	public static GetUser convert(User user) {
		GetUser getUser = new GetUser();
		
		getUser.setId(user.getId());
		
		return getUser;
	}
}
