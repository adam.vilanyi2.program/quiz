package hu.clarmont.Quiz.controller.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SetAnswer {
	@NotNull
	long userId;
	
	@NotBlank
	String answer;
}
