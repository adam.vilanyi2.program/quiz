package hu.clarmont.Quiz.controller;

import hu.clarmont.Quiz.controller.dto.*;
import hu.clarmont.Quiz.controller.entity.Question;
import hu.clarmont.Quiz.controller.entity.Result;
import hu.clarmont.Quiz.controller.entity.User;
import hu.clarmont.Quiz.service.ResultService;
import hu.clarmont.Quiz.service.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import hu.clarmont.Quiz.service.QuestionService;

import java.util.List;

@RestController
public class QuizController {
	@Autowired
	QuestionService questionService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	ResultService resultService;
	
	
	@GetMapping("/{id}")
	public Question getById(@PathVariable long id) {
		return questionService.getById(id);
	}
	
	@GetMapping()
	public List<Question> getAll() {
		return questionService.getAll();
	}
	
	@GetMapping("/random")
	public GetQuestion getRandom() {
		return questionService.getRandom();
	}
	
	@PostMapping("/{id}")
	public ResponseEntity<?> checkAnswer(@PathVariable long id, @RequestBody @Valid SetAnswer answer) {
		User user = userService.getUser(answer.getUserId());
		if (user == null) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No such user");
		
		Question question = questionService.getById(id);
		if (question == null) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("No such question");
		
		int points = questionService.checkAnswer(id, answer.getAnswer()) ? 10 : 0;
		Result result = resultService.addResult(user, question, points);
		
		return ResponseEntity.status(200).body(GetResult.convert(result));
	}
	
	@PostMapping("/")
	public void createQuestion(@RequestBody @Valid SetQuestion question) {
		questionService.createQuestion(question);
	}
}
