package hu.clarmont.Quiz.repository;

import hu.clarmont.Quiz.controller.entity.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long>, PagingAndSortingRepository<Question, Long>, JpaSpecificationExecutor<Question> {
	@Query("SELECT q.id FROM Question q")
	List<Long> getIDs();
}
