package hu.clarmont.Quiz.repository;

import hu.clarmont.Quiz.controller.entity.Result;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultRepository extends JpaRepository<Result, Long>, PagingAndSortingRepository<Result, Long>, JpaSpecificationExecutor<Result> {

}
